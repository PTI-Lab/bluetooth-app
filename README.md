# RIBBO LAUNCHER APP #

### What is this repository for? ###

This repository contains the source files for the Ribbo launcher in Android. Using bluetooth, it search for available beams. It can also subscribe to a GCM service and receive remote orders to control the robot. It can also be controlled manually from the application.

### How do I get set up? ###

1. We need to have Android Studio in our machine. You can download it from [here](http://developer.android.com/intl/es/sdk/index.html)
2. We also need to have our Android SDK set up for API 21 (IT MUST BE version 21!)
3. Clone the repository: **git clone https://rioardila@bitbucket.org/PTI-Lab/bluetooth-app.git** 
4. Start Android Studio and import the Android Studio project
5. Make sure again you have Android API v. 21, then you can build the project and test it