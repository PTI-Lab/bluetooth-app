// Copyright Google Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.ainasg.ptillumetes;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.ainasg.ptillumetes.constants.RegistrationConstants;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String REGISTER_URL = "http://ribbo.tk/tokenregister/";
    private static final String KEY_TOKEN = "gcm_token";
    private static final String senderId = "57593720750";

    public RegistrationIntentService() {
        super(TAG);
    }

    private Handler handler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler = new Handler();
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        String token;

        Intent regCompleteIntent = new Intent(RegistrationConstants.REGISTRATION_COMPLETE);

        try {
            // Initially this call goes out to the network to retrieve the token, subsequent
            // calls are local.
            String string_identifier = extras.getString(RegistrationConstants.STRING_IDENTIFIER);

            SharedPreferences prefs = getSharedPreferences("Preferencies", Context.MODE_PRIVATE);
            if(prefs.getString("TokenID",null) == null) { //si encara no te cap token de google
                //Demana el Token a Google
                token = InstanceID.getInstance(this).getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.d(TAG, "GCM Registration Token: " + token);
                //guardem el token a les Preferencies
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("TokenID", token);
                editor.apply();
            }
            else {
                token = prefs.getString("TokenID","");
                Log.d(TAG, "GCM Registration Token(saved) " + token);
            }

            // Register token with app server.
            String robot_name = prefs.getString("RobotID", "");
            String result = sendRegistrationToServer(token, string_identifier, robot_name);
            Log.d(TAG, "result_message:" + result);

            // You should store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            regCompleteIntent.putExtra(RegistrationConstants.SENT_TOKEN_TO_SERVER, true);
        } catch (Exception e) {
            Log.e(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration
            // data on a third-party server, this ensures that we'll attempt the update at a later
            // time.
            regCompleteIntent.putExtra(RegistrationConstants.SENT_TOKEN_TO_SERVER, false);
        }

        //Send the broadcast
        Log.d(TAG, "Sending the broadcast");
        LocalBroadcastManager.getInstance(this).sendBroadcast(regCompleteIntent);
    }

        /**
     * Register a GCM registration token with the app server
     * @param token Registration token to be registered
     * @param string_identifier A human-friendly name for the client
     * @return true if request succeed
     * @throws IOException
     */
        private String sendRegistrationToServer(String token, String string_identifier, String robot_name) throws IOException {
        Bundle registration = createRegistrationBundle(token, string_identifier);
            String result = "Something went wrong";
            Boolean error = true;

        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new FormEncodingBuilder()
                .add("token", token)
                .add("name", robot_name)
                .build();

        Request request = new Request.Builder()
                .url(REGISTER_URL)
                .post(requestBody)
                .build();

        try {
            Response response = client.newCall(request).execute();
            Log.d(TAG,"Request: " + request.toString());
            Log.d(TAG, "Response code: " + response.code());
            try {
                JSONObject object = new JSONObject(response.body().string());
                error = object.getBoolean("error");
                result = object.getString("message");
                Log.d(TAG, "Error " + error);
                Log.d(TAG, "Message " + result);

            } catch (final JSONException e) {
                error = true;
                Log.d(TAG, "JSON OBJECT EXCEPTION" + e.getMessage());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "JSON OBJECT EXCEPTION: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
            //Checks if register was successfully and show it in a toast
            if(!error) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Registered successfully!", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Unexpected error. Please try it again.", Toast.LENGTH_SHORT).show();
                    }
                });

                Boolean exit = false;
                while(!exit) {
                    try {
                        Thread.sleep(2000);
                        exit = true;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //go back to main activity
                restartFirstActivity();

            }
            return result;
    }

    /**
     * Creates the registration bundle and fills it with user information
     * @param token Registration token to be registered
     * @param string_identifier A human-friendly name for the client
     * @return A bundle with registration data.
     */
    private Bundle createRegistrationBundle(String token, String string_identifier) {
        Bundle registration = new Bundle();

        // Create the bundle for registration with the server.
        registration.putString(RegistrationConstants.ACTION, RegistrationConstants.REGISTER_NEW_CLIENT);
        registration.putString(RegistrationConstants.REGISTRATION_TOKEN, token);
        registration.putString(RegistrationConstants.STRING_IDENTIFIER, string_identifier);
        return registration;
    }

    //this function restarts the app and goes back to the main activity
    private void restartFirstActivity()
    {
        Intent i = getApplicationContext().getPackageManager()
                .getLaunchIntentForPackage(getApplicationContext().getPackageName() );

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity(i);
    }

}
