package com.ainasg.ptillumetes;

import android.content.Context;

import com.punchthrough.bean.sdk.Bean;

/**
 * Created by aina on 09/04/16.
 */
public class MyBean  {
    public String name;
    public String mac;
    public Bean bean;

    public MyBean(Bean bean, Context context){
        this.bean = bean;
        this.name = bean.getDevice().getName();
        this.mac = bean.getDevice().getAddress();
    }
}
