package com.ainasg.ptillumetes;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.punchthrough.bean.sdk.BeanListener;
import com.punchthrough.bean.sdk.message.BeanError;
import com.punchthrough.bean.sdk.message.ScratchBank;

import java.util.ArrayList;

/**
 * Created by aina on 09/04/16.
 */
public class BeanAdapter extends ArrayAdapter<MyBean> {
    Context context;
    public BeanAdapter(Context context, ArrayList<MyBean> beans) {
        super(context, 0, beans);
        this.context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final MyBean bean = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.bean_list_item, parent, false);
        }

        TextView beanName = (TextView) convertView.findViewById(R.id.beanname);
        TextView beanAddress = (TextView) convertView.findViewById(R.id.beanaddress);

        beanName.setText(bean.name);
        beanAddress.setText(bean.mac);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Click on " + bean.name,
                        Toast.LENGTH_SHORT).show();
                //Intent myIntent = new Intent(context, BeanAdapter.class);
                //myIntent.putExtra("actual_bean", bean); //Optional parameters
                //context.startActivity(myIntent);
                BeanListener thisbean = new BeanListener() {
                    @Override
                    public void onConnected() {
                        Log.d("Connection77", "connection OK");

                        Intent myIntent = new Intent(context, ControlActivity.class);
                        myIntent.putExtra("wich_bean", position);
                        context.startActivity(myIntent);
                    }

                    @Override
                    public void onConnectionFailed() {
                        Log.d("Connection77", "connection FAIL");

                    }

                    @Override
                    public void onDisconnected() {
                        Log.d("Connection77", "connection BYEBYE");

                    }

                    @Override
                    public void onSerialMessageReceived(byte[] bytes) {

                    }

                    @Override
                    public void onScratchValueChanged(ScratchBank scratchBank, byte[] bytes) {

                    }

                    @Override
                    public void onError(BeanError beanError) {
                        Log.d("Connection77", "connection ERROR");

                    }
                };
                bean.bean.connect(context, thisbean);
               // bean.bean.sendSerialMessage("#back;");
            }
        });
        return convertView;
    }


}

