package com.ainasg.ptillumetes;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ainasg.ptillumetes.constants.RegistrationConstants;
import com.punchthrough.bean.sdk.Bean;
import com.punchthrough.bean.sdk.BeanDiscoveryListener;
import com.punchthrough.bean.sdk.BeanManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends ActionBarActivity {

    private static final String senderId = "57593720750";
    public static int REQUEST_BLUETOOTH = 1;
    public static ArrayList<MyBean> discovered = new ArrayList<>();
    BeanDiscoveryListener listener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView llista = (ListView) findViewById(R.id.beanlist);
        final BeanAdapter adapter = new BeanAdapter(this, discovered);

        llista.setAdapter(adapter);

        FancyButton but = (FancyButton) findViewById(R.id.buttonsearch);

        //Last name written is showned
        final EditText nom = (EditText) findViewById(R.id.robotName);
        SharedPreferences prefs = getSharedPreferences("Preferencies",Context.MODE_PRIVATE);
        nom.setText(prefs.getString("RobotID",""));

        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nom.getText().length() == 0) {
                    Toast.makeText(MainActivity.this, "Must specify a name for the robot!",
                            Toast.LENGTH_SHORT).show();
                }
                else if (!isBluetoothAvailable()) {
                    Toast.makeText(MainActivity.this, "Please enable Bluetooth",
                            Toast.LENGTH_SHORT).show();
                    Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBT, REQUEST_BLUETOOTH);
                }
                else {
                    //we save robot's name in Preferencies
                    SharedPreferences prefs = getSharedPreferences("Preferencies",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("RobotID",nom.getText().toString());
                    editor.commit();

                    final List<Bean> beans = new ArrayList<>();
                    Toast.makeText(MainActivity.this, "Searching BT beans...",
                            Toast.LENGTH_SHORT).show();
                    listener = new BeanDiscoveryListener() {
                        @Override
                        public void onBeanDiscovered(Bean bean, int i) {
                            beans.add(bean);
                            discovered.add(new MyBean(bean, MainActivity.this));
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onDiscoveryComplete() {
                            for (Bean bean : beans) { }
                            Toast.makeText(MainActivity.this, "Discover finished!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    };
                    discovered.clear();
                    adapter.notifyDataSetChanged();
                    BeanManager.getInstance().startDiscovery(listener);

                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //this function returns true if Bluetooth is available and enabled. Otherwise, returns false
    public static boolean isBluetoothAvailable() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

}
