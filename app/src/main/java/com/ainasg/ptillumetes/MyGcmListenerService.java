package com.ainasg.ptillumetes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.ainasg.ptillumetes.constants.RegistrationConstants;
import com.google.android.gms.gcm.GcmListenerService;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);

        String message = data.getString("message");
        String status=data.getString("status");
        //  String message="";
        /*  for (String key : data.keySet()) {
            message += " " + key + " => " + data.get(key) + ";";
        }*/
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        Log.d(TAG, "Status: " + status);

        Intent downstreamMessageIntent = new Intent(RegistrationConstants.NEW_DOWNSTREAM_MESSAGE);
        downstreamMessageIntent.putExtra(RegistrationConstants.SENDER_ID, from);
        downstreamMessageIntent.putExtra(RegistrationConstants.EXTRA_KEY_BUNDLE, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(downstreamMessageIntent);

    }


}
