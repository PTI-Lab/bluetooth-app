package com.ainasg.ptillumetes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ainasg.ptillumetes.constants.RegistrationConstants;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;


public class ControlActivity extends ActionBarActivity {
    private static final String senderId = "57593720750";
    private static final String TAG = "" ;
    private BroadcastReceiver mDownstreamBroadcastReceiver;
    private GoogleCloudMessaging gcm;
    private GcmPubSub pubSub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_control, menu);

        Intent intent = getIntent();

        final MyBean bean = MainActivity.discovered.get(intent.getIntExtra("wich_bean", 0));

        TextView beanName = (TextView) findViewById(R.id.beancontr);

        beanName.setText(bean.name);

        final Button red = (Button) findViewById(R.id.red);
        final Button blue = (Button) findViewById(R.id.blue);
        final Button green = (Button) findViewById(R.id.green);
        final Button yellow = (Button) findViewById(R.id.yellow);
        final Button orange = (Button) findViewById(R.id.orange);

        gcm = GoogleCloudMessaging.getInstance(this);
        pubSub = GcmPubSub.getInstance(this);

        // each pressed button sends an order through BT
        red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.bean.sendSerialMessage("#back;");
            }
        });

        blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.bean.sendSerialMessage("#front;");
            }
        });

        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.bean.sendSerialMessage("#tr;");
            }
        });

        orange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.bean.sendSerialMessage("#stop;");
            }
        });

        yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bean.bean.sendSerialMessage("#tl;");
            }
        });

        //while contacting server show a toast
        Toast.makeText(ControlActivity.this, "Contacting Server. Please wait...",
                Toast.LENGTH_SHORT).show();

        //get robot's name
        SharedPreferences prefs = getSharedPreferences("Preferencies",Context.MODE_PRIVATE);

        //register robot
        registerClient(prefs.getString("RobotID",""));

        mDownstreamBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String from = intent.getStringExtra(RegistrationConstants.SENDER_ID);
                String message = intent.getStringExtra(RegistrationConstants.EXTRA_KEY_BUNDLE);

                //New message received
                Toast.makeText(ControlActivity.this, "Order: " + message,
                        Toast.LENGTH_SHORT).show();
                //Send orders to the robot
                if (message.equals("UP")){
                    blue.callOnClick();
                }
                if (message.equals("DOWN")){
                    red.callOnClick();
                }
                if (message.equals("LEFT")){
                    yellow.callOnClick();
                }
                if (message.equals("RIGHT")){
                    green.callOnClick();
                }
                if (message.equals("STOP")){
                    orange.callOnClick();
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mDownstreamBroadcastReceiver,
                new IntentFilter(RegistrationConstants.NEW_DOWNSTREAM_MESSAGE));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void registerClient(String stringId) {
        // Register with GCM
        Intent intent = new Intent(this, RegistrationIntentService.class);
        intent.putExtra(RegistrationConstants.SENDER_ID, senderId);
        intent.putExtra(RegistrationConstants.STRING_IDENTIFIER, stringId);

        startService(intent);
    }
}
